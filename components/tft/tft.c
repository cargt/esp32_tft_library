/* TFT module
 *
 *  Author: LoBo (loboris@gmail.com, loboris.github)
 *
 *  Module supporting SPI TFT displays based on ILI9341 & ILI9488 controllers
*/

#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "tft.h"
#include "time.h"
#include <math.h>
#include "rom/tjpgd.h"
#include "esp_heap_caps.h"
#include "tftspi.h"


#define DEG_TO_RAD 0.01745329252
#define RAD_TO_DEG 57.295779513
#define deg_to_rad 0.01745329252 + 3.14159265359
#define swap(a, b) { int16_t t = a; a = b; b = t; }
#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))
#if !defined(max)
#define max(A,B) ( (A) > (B) ? (A):(B))
#endif
#if !defined(min)
#define min(A,B) ( (A) < (B) ? (A):(B))
#endif

// Embedded fonts
extern uint8_t tft_SmallFont[];
extern uint8_t tft_DefaultFont[];
extern uint8_t tft_Dejavu18[];
extern uint8_t tft_Dejavu24[];
extern uint8_t tft_Ubuntu16[];
extern uint8_t tft_Comic24[];
extern uint8_t tft_minya24[];
extern uint8_t tft_tooney32[];
extern uint8_t tft_def_small[];

// ==== Color definitions constants ==============
const color_t TFT_BLACK       = {   0,   0,   0 };
const color_t TFT_NAVY        = {   0,   0, 128 };
const color_t TFT_DARKGREEN   = {   0, 128,   0 };
const color_t TFT_DARKCYAN    = {   0, 128, 128 };
const color_t TFT_MAROON      = { 128,   0,   0 };
const color_t TFT_PURPLE      = { 128,   0, 128 };
const color_t TFT_OLIVE       = { 128, 128,   0 };
const color_t TFT_LIGHTGREY   = { 192, 192, 192 };
const color_t TFT_DARKGREY    = { 128, 128, 128 };
const color_t TFT_BLUE        = {   0,   0, 255 };
const color_t TFT_GREEN       = {   0, 255,   0 };
const color_t TFT_CYAN        = {   0, 255, 255 };
const color_t TFT_RED         = { 252,   0,   0 };
const color_t TFT_MAGENTA     = { 252,   0, 255 };
const color_t TFT_YELLOW      = { 252, 252,   0 };
const color_t TFT_WHITE       = { 252, 252, 252 };
const color_t TFT_ORANGE      = { 252, 164,   0 };
const color_t TFT_GREENYELLOW = { 172, 252,  44 };
const color_t TFT_PINK        = { 252, 192, 202 };
// ===============================================

// ==============================================================
// ==== Set default values of global variables ==================
uint8_t orientation = LANDSCAPE;// screen orientation
uint16_t font_rotate = 0;		// font rotation
uint8_t	font_transparent = 0;
uint8_t	font_forceFixed = 0;
uint8_t	text_wrap = 0;			// character wrapping to new line
color_t	_fg = {  0, 255,   0};
color_t _bg = {  0,   0,   0};
uint8_t image_debug = 0;

float _angleOffset = DEFAULT_ANGLE_OFFSET;

int	TFT_X = 0;
int	TFT_Y = 0;

uint32_t tp_calx = 7472920;
uint32_t tp_caly = 122224794;

dispWin_t dispWin = {
  .x1 = 0,
  .y1 = 0,
  .x2 = DEFAULT_TFT_DISPLAY_WIDTH,
  .y2 = DEFAULT_TFT_DISPLAY_HEIGHT,
};

Font cfont = {
	.font = tft_DefaultFont,
	.x_size = 0,
	.y_size = 0x0B,
	.offset = 0,
	.numchars = 95,
	.bitmap = 1,
};

uint8_t font_buffered_char = 1;
uint8_t font_line_space = 0;
// ==============================================================


typedef struct {
      uint8_t charCode;
      int adjYOffset;
      int width;
      int height;
      int xOffset;
      int xDelta;
      uint16_t dataPtr;
} propFont;

static dispWin_t dispWinTemp;

static uint8_t *userfont = NULL;
static int TFT_OFFSET = 0;
static propFont	fontChar;
static float _arcAngleMax = DEFAULT_ARC_ANGLE_MAX;



// ================ Service functions ==========================================

// Change the screen rotation.
// Input: m new rotation value (0 to 3)
//=================================
void TFT_setRotation(uint8_t rot) {
    if (rot > 3) {
        uint8_t madctl = (rot & 0xF8); // for testing, manually set MADCTL register
		if (disp_select() == ESP_OK) {
			disp_spi_transfer_cmd_data(TFT_MADCTL, &madctl, 1);
			disp_deselect();
		}
    }
	else {
		orientation = rot;
        _tft_setRotation(rot);
	}

	dispWin.x1 = 0;
	dispWin.y1 = 0;
	dispWin.x2 = _width-1;
	dispWin.y2 = _height-1;

}

// Send the command to invert all of the colors.
// Input: i 0 to disable inversion; non-zero to enable inversion
//==========================================
void TFT_invertDisplay(const uint8_t mode) {
  if ( mode == INVERT_ON ) disp_spi_transfer_cmd(TFT_INVONN);
  else disp_spi_transfer_cmd(TFT_INVOFF);
}

// Select gamma curve
// Input: gamma = 0~3
//==================================
void TFT_setGammaCurve(uint8_t gm) {
  uint8_t gamma_curve = 1 << (gm & 0x03);
  disp_spi_transfer_cmd_data(TFT_CMD_GAMMASET, &gamma_curve, 1);
}





// ============= Touch panel functions =========================================

#if USE_TOUCH == TOUCH_TYPE_XPT2046
//-------------------------------------------------------
static int tp_get_data_xpt2046(uint8_t type, int samples)
{
	if (ts_spi == NULL) return 0;

	int n, result, val = 0;
	uint32_t i = 0;
	uint32_t vbuf[18];
	uint32_t minval, maxval, dif;

    if (samples < 3) samples = 1;
    if (samples > 18) samples = 18;

    // one dummy read
    result = touch_get_data(type);

    // read data
	while (i < 10) {
    	minval = 5000;
    	maxval = 0;
		// get values
		for (n=0;n<samples;n++) {
		    result = touch_get_data(type);
			if (result < 0) break;

			vbuf[n] = result;
			if (result < minval) minval = result;
			if (result > maxval) maxval = result;
		}
		if (result < 0) break;
		dif = maxval - minval;
		if (dif < 40) break;
		i++;
    }
	if (result < 0) return -1;

	if (samples > 2) {
		// remove one min value
		for (n = 0; n < samples; n++) {
			if (vbuf[n] == minval) {
				vbuf[n] = 5000;
				break;
			}
		}
		// remove one max value
		for (n = 0; n < samples; n++) {
			if (vbuf[n] == maxval) {
				vbuf[n] = 5000;
				break;
			}
		}
		for (n = 0; n < samples; n++) {
			if (vbuf[n] < 5000) val += vbuf[n];
		}
		val /= (samples-2);
	}
	else val = vbuf[0];

    return val;
}

//-----------------------------------------------
static int TFT_read_touch_xpt2046(int *x, int* y)
{
	int res = 0, result = -1;
	if (spi_lobo_device_select(ts_spi, 0) != ESP_OK) return 0;

    result = tp_get_data_xpt2046(0xB0, 3);  // Z; pressure; touch detect
	if (result <= 50) goto exit;

	// touch panel pressed
	result = tp_get_data_xpt2046(0xD0, 10);
	if (result < 0)  goto exit;

	*x = result;

	result = tp_get_data_xpt2046(0x90, 10);
	if (result < 0)  goto exit;

	*y = result;
	res = 1;
exit:
	spi_lobo_device_deselect(ts_spi);
	return res;
}
#endif

//=============================================
int TFT_read_touch(int *x, int* y, uint8_t raw)
{
    *x = 0;
    *y = 0;
	if (ts_spi == NULL) return 0;
    #if USE_TOUCH == TOUCH_TYPE_NONE
	return 0;
    #else
	int result = -1;
    int X=0, Y=0;

    #if USE_TOUCH == TOUCH_TYPE_XPT2046
    uint32_t tp_calx = TP_CALX_XPT2046;
    uint32_t tp_caly = TP_CALY_XPT2046;
   	result = TFT_read_touch_xpt2046(&X, &Y);
   	if (result == 0) return 0;
    #elif USE_TOUCH == TOUCH_TYPE_STMPE610
    uint32_t tp_calx = TP_CALX_STMPE610;
    uint32_t tp_caly = TP_CALY_STMPE610;
    uint16_t Xx, Yy, Z=0;
    result = stmpe610_get_touch(&Xx, &Yy, &Z);
    if (result == 0) return 0;
    X = Xx;
    Y = Yy;
	#elif USE_TOUCH == TOUCH_TYPE_SX8652
	uint32_t tp_calx = TP_CALX_SX8652;
	uint32_t tp_caly = TP_CALY_SX8652;
	uint16_t Xx, Yy, Z=0;
	result = sx8652_get_touch(&Xx, &Yy, &Z);
	if (result == 0) return 0;
	X = Xx;
	Y = Yy;
    #else
    return 0;
    #endif

    if (raw) {
    	*x = X;
    	*y = Y;
    	return 1;
    }

    // Calibrate the result
	int tmp;
	int xleft   = (tp_calx >> 16) & 0x3FFF;
	int xright  = tp_calx & 0x3FFF;
	int ytop    = (tp_caly >> 16) & 0x3FFF;
	int ybottom = tp_caly & 0x3FFF;

	if (((xright - xleft) <= 0) || ((ybottom - ytop) <= 0)) return 0;

    #if USE_TOUCH == TOUCH_TYPE_XPT2046
        int width = _width;
        int height = _height;
        X = ((X - xleft) * height) / (xright - xleft);
        Y = ((Y - ytop) * width) / (ybottom - ytop);

        if (X < 0) X = 0;
        if (X > height-1) X = height-1;
        if (Y < 0) Y = 0;
        if (Y > width-1) Y = width-1;

        switch (orientation) {
            case PORTRAIT:
                tmp = X;
                X = width - Y - 1;
                Y = tmp;
                break;
            case PORTRAIT_FLIP:
                tmp = X;
                X = Y;
                Y = height - tmp - 1;
                break;
            case LANDSCAPE_FLIP:
                X = height - X - 1;
                Y = width - Y - 1;
                break;
        }
    #elif USE_TOUCH == TOUCH_TYPE_STMPE610
        int width = _width;
        int height = _height;
        if (_width > _height) {
            width = _height;
            height = _width;
        }
		X = ((X - xleft) * width) / (xright - xleft);
		Y = ((Y - ytop) * height) / (ybottom - ytop);

		if (X < 0) X = 0;
		if (X > width-1) X = width-1;
		if (Y < 0) Y = 0;
		if (Y > height-1) Y = height-1;

		switch (orientation) {
			case PORTRAIT_FLIP:
				X = width - X - 1;
				Y = height - Y - 1;
				break;
			case LANDSCAPE:
				tmp = X;
				X = Y;
				Y = width - tmp -1;
				break;
			case LANDSCAPE_FLIP:
				tmp = X;
				X = height - Y -1;
				Y = tmp;
				break;
		}

#elif USE_TOUCH == TOUCH_TYPE_SX8652
    int width = _width;
    int height = _height;
	int Xtemp, Ytemp;


	Xtemp = ((X - xleft) * height) / (xright - xleft);
	Ytemp = ((Y - ytop) * width) / (ybottom - ytop);

	if (Xtemp < 0) Xtemp = 0;
	if (Xtemp > height-1) Xtemp = height-1;
	if (Ytemp < 0) Ytemp = 0;
	if (Ytemp > width-1) Ytemp = width-1;

	if( TP_SX8652_REVERSE_H == 1 )
	{
		Xtemp = height-1 - Xtemp;
	}
	if( TP_SX8652_REVERSE_W == 1 )
	{
		Ytemp = width-1 - Ytemp;
	}

	if( TP_SX8652_SWAP_XY == 0 )
	{
		X = Xtemp;
		Y = Ytemp;
	}
	else
	{
		Y = Xtemp;
		X = Ytemp;
	}



    #endif
	*x = X;
	*y = Y;
	return 1;
    #endif
}

