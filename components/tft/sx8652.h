/*
    SX8652 Touch controller constants
*/

#ifndef _SX8652_H
#define _SX8652_H

#include <stdint.h>

#define SX8652_SPI_MODE      		0
#define SX8652_SPI_CS_ENA_PRETRANS 	1

// commands
#define SX8652_READ_REG			0x40
#define SX8652_READ_CH			0x20
#define SX8652_WRITE_REG		0x00
#define SX8652_SELECT_CH		0x80
#define SX8652_CONVERT_CH		0x90
#define SX8652_MANAUTO			0xB0
#define SX8652_PENDET 			0xC0
#define SX8652_PENTRG 			0xE0

// channels
#define SX8652_CH_X				0x00
#define SX8652_CH_Y				0x01
#define SX8652_CH_Z1			0x02
#define SX8652_CH_Z2			0x03
#define SX8652_CH_AUX			0x04
#define SX8652_CH_res1			0x05
#define SX8652_CH_res2			0x06
#define SX8652_CH_SEQ			0x07

// registers
#define SX8652_REG_CTRL0      	0x00
#define SX8652_REG_CTRL1       	0x01
#define SX8652_REG_CTRL2       	0x02
#define SX8652_REG_CH_MASK     	0x04
#define SX8652_REG_STATUS      	0x05
#define SX8652_REG_SOFT_RESET  	0x1F


//SX8652_REG_STATUS
#define SX8652_REG_STATUS_CONVIRQ_bit	7
#define SX8652_REG_STATUS_PENIRQ_bit	6
#define SX8652_REG_STATUS_RSTEVENT_bit	5

//SX8652_REG_SOFT_RESET
#define SX8652_REG_SOFT_RESET_value  	0xDE

#endif /* _SX8652_H */
